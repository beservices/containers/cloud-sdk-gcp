FROM debian:latest

LABEL description="Image with GCP SDK plus terraform installation"

# Surpress Upstart errors/warning
RUN dpkg-divert --local --rename --add /sbin/initctl
RUN ln -sf /bin/true /sbin/initctl

# Let the conatiner know that there is no tty
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get dist-upgrade -y && \
    apt-get install -y --no-install-recommends bash apt-transport-https ca-certificates \
    vim at curl gnupg net-tools git ssh-client tree groff jq lsb-release unzip && \
    rm -rf /var/lib/apt/list/*

RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | tee /usr/share/keyrings/cloud.google.gpg && \
    apt-get update && apt-get install -y google-cloud-cli google-cloud-cli-terraform-validator kubectl peco && \
    curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -  && \
    echo "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee -a /etc/apt/sources.list.d/hashicorp.list && \
    apt-get update &&  apt-get install -y terraform && \
    rm -rf /var/lib/apt/lists/* /var/cache/archives/*.deb 

WORKDIR /root
